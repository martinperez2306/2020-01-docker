#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1
NGINX_PORT=80

#docker run --rm -d hello-world
docker run --rm --name nginx-server -v "$(pwd)/files:/usr/share/nginx/html" -v "$(pwd)/nginx.conf:/etc/nginx/nginx.conf" -d -p $TEST_PORT:$NGINX_PORT nginx
